class CreateCustomercontacts < ActiveRecord::Migration[5.2]
  def change
    create_table :customer_contacts do |t|
      t.string :customer_name
      t.string :shop_name
      t.string :phone
      t.string :email
      t.string :question
      t.timestamps
    end
  end
end
