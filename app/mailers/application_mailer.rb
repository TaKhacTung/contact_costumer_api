class ApplicationMailer < ActionMailer::Base
  # default from: 'thatthanki@gmail.com'
  layout 'customer_mailer'
end
