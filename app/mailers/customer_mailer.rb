class CustomerMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.customer_mailer.mailer.subject
  #
  def mailer(customer)
    @customer = customer
   mail(to: @customer.email, subject: 'Email Contact')
  end
end