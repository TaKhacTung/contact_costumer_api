class CustomerContact < ApplicationRecord
  validates :customer_name, presence: true
  validates :shop_name, presence: true
  validates :email, presence: true, format: /\A[a-zA-Z0-9_\-\.]+@(([a-zA-Z]+\.[a-zA-Z]+)|(([0-9]\.){3}[0-9]))\z/
  validates :phone, presence: true, length: { in: 9..11 }
end
