module Api
  module V1
    class CustomerService
      def initialize(customer)
        @customer = customer
      end

      def create(attribute)
        customer.assign_attributes(attribute)
        CustomerMailer.mailer(customer).deliver_now if customer.save!
      end

      def update(attribute)
        customer.assign_attributes(attribute)
        customer.save!
      end

      def destroy
        customer.destroy
      end

      private

      attr_accessor :customer
    end
  end
end