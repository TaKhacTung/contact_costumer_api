module Api
  module V1
    class CustomerContactController < BaseController
      def create
        @customer = CustomerContact.new
        services.create(customer_params)
        render json: { data: @customer,message: "Contact is successfully sent"}, status: :created
      end

      def show
        @customer = CustomerContact.find_by_id(params[:id])
        render json: { data: @customer}
      end

      def update
        @customer = CustomerContact.find_by_id(params[:id])
        services.update(customer_params)
        render json: { data: @customer,message: "update successful"}
      end

      def destroy
        @customer = CustomerContact.find_by_id(params[:id])
        services.destroy
        render json: { data: @customer, message: "Contact information has been deleted" }, status: :ok
      end

      private

      def customer_params
        params[:customer_contact].permit(:customer_name,:shop_name,:phone,:email,:question)
      end

      def services
        Api::V1::CustomerService.new(@customer)
      end
    end
  end
end
